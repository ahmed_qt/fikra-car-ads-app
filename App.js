import React from 'react';
// my imports
import { createStackNavigator, createAppContainer } from 'react-navigation';
import LoginScreen from './components/LoginScreen';
import WorkScreen from './components/WorkScreen';


console.disableYellowBox = true;

const AppNavigator = createStackNavigator({
  Home: WorkScreen,
  Login: LoginScreen
},
{
    navigationOptions: {
      header: null
    },
    headerMode: "none",
    initialRouteName: 'Login'
});


const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  
  componentWillMount() {
  }

  componentWillUnmount() {
  }

  render() {
    return (
      <AppContainer/>
    );
  }
}


