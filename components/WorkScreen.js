import React from 'react';
import { Modal, ActivityIndicator, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Text, Image, View, Dimensions, StatusBar, NetInfo, AsyncStorage } from 'react-native';
import { Icon } from 'native-base';
import { Video, FileSystem, ScreenOrientation } from 'expo';
import myconfig from '../config';
import Modall from "react-native-modal";

// disabled many features for simplicity need to clean up later
export default class WorkScreen extends React.Component {


    player; // ref for video component
    token;

    state = {
        ads: [],
        ads_playlist: [],
        user_ads: [],
        current_ad_index: 0,
        current_ad_image: null,
        download_index: 0,
        loading_text: 'Downloading ads files...',
        ad_type: 1,
        loading: true,
        is_playing: false,
        internet_error: false,
        showMenu: false,
    }

    async sync() {
        return new Promise(async resolve => {
            // console.log(this.state.user_ads)
            let user_ads = this.state.user_ads.filter(e => e.status != 1);
            resolve(true)
            // await AsyncStorage.setItem("user_ads", user_ads);
            // this.setState({ user_ads: user_ads }, () => {
            //     resolve(true);
            // });
        })
    }

    checkapi() {
        setInterval(async () => {
            console.log("checking api for updates")
            const isConnected = await NetInfo.isConnected.fetch()
            const local_ads = await AsyncStorage.getItem("ads");
            if (local_ads != null) {
                this.setState({ ads: JSON.parse(local_ads) });
            }
            if (!isConnected) {
                this.processFilesOffline(this.state.ads);
            }
            try {
                console.log("syncing database with server")
                // await this.sync();
                console.log("Hey Iam here");
                const ads = await fetch(myconfig.backend_url + "/api/ad/", { headers: { "Authorization": this.token } }).then(res => res.json())
                await AsyncStorage.setItem("ads", JSON.stringify(ads));
                console.log(ads)
                // const user_ads = await AsyncStorage.getItem("user_ads");
                // if (user_ads != null) {
                //     // this.setState({ user_ads: JSON.parse(user_ads) })
                // }
                filesDir = await FileSystem.getInfoAsync(FileSystem.documentDirectory + "files/");
                if (filesDir.exists && filesDir.isDirectory) {
                    this.processFiles(ads);
                } else {
                    console.log("Files dir not exists");
                    console.log("Creating it...");
                    await FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + "files/");
                    this.processFiles(ads);
                }
            } catch (e) {
                console.log('error occurred', e);
            }
        }, 5000)
    }

    // fetch ads from api and process each ad
    async load() {
        const isConnected = await NetInfo.isConnected.fetch()
        this.token = await AsyncStorage.getItem("token");
        const local_ads = await AsyncStorage.getItem("ads");
        if (local_ads != null) {
            console.log(local_ads)
            this.setState({ ads: JSON.parse(local_ads) });
        }
        if (!isConnected) {
            // this.processFilesOffline(this.state.ads);
            return
        }
        try {
            console.log('Iam here')
            const ads = await fetch(myconfig.backend_url + "/api/ad/", { headers: { "Authorization": this.token } }).then(res => res.json())
            await AsyncStorage.setItem("ads", JSON.stringify(ads));
            const user_ads = await AsyncStorage.getItem("user_ads");
            if (user_ads != null) {
                this.setState({ user_ads: JSON.parse(user_ads) })
            }
            // this.checkapi();
            const filesDir = await FileSystem.getInfoAsync(FileSystem.documentDirectory + "files/");
            if (filesDir.exists && filesDir.isDirectory) {
                this.processFiles(ads);
            } else {
                console.log("Files dir not exists");
                console.log("Creating it...");
                await FileSystem.makeDirectoryAsync(FileSystem.documentDirectory + "files/");
                this.processFiles(ads);
            }
        } catch (e) {
            console.log('error occurred', e);
        }
    }

    // download ad files and skip file if exist and whens md5 is correct
    async processFiles(ads) {
        let ads_playlist = [];
        for (const ad of ads) {
            this.setState({
                loading_text: 'Downloading ads files',
                //  (' + this.state.download_index + 1 + ") of (" + this.state.ads.length + ")"
            })
            const info = await FileSystem.getInfoAsync(FileSystem.documentDirectory + "files/" + ad.video, { md5: true });
            if (info.exists) {
                // && info.md5 == ad.md5_checksum
                // console.log("already downloaded and passed md5 check");
                const local_uri = await FileSystem.downloadAsync(myconfig.backend_url + "/api/files/" + ad.video, FileSystem.documentDirectory + "/files/" + ad.video);
                console.log(myconfig.backend_url + "/api/files/" + ad.video)
                ads_playlist.push(ad);
            } else {
                const local_uri = await FileSystem.downloadAsync(myconfig.backend_url + "/api/files/" + ad.video, FileSystem.documentDirectory + "/files/" + ad.video);
                ads_playlist.push(ad);
            }
        }
        this.setState({ ads_playlist, loading: false })
        this.play();
    }

    async processFilesOffline(ads) {
        let ads_playlist = [];
        for (const ad of ads) {
            const info = await FileSystem.getInfoAsync(FileSystem.documentDirectory + "files/" + ad.video, { md5: true });
            if (info.exists) {
                // && info.md5 == ad.md5_checksum
                // console.log("already downloaded and passed md5 check");
                ads_playlist.push(ad);
            }
        }
        this.setState({ ads_playlist, loading: false })
        this.play();
    }

    async play() {
        console.log("play")
        if (this.state.is_playing == false) {
            this.setState({ is_playing: true }, async () => {
                if (this.state.ads_playlist[this.state.current_ad_index] == undefined) {
                    return;
                } else {
                    console.log(FileSystem.documentDirectory + "files/" + this.state.ads_playlist[this.state.current_ad_index].video)
                    const file = FileSystem.documentDirectory + "files/" + this.state.ads_playlist[this.state.current_ad_index].video;
                    const info = await FileSystem.getInfoAsync(FileSystem.documentDirectory + "files/" + this.state.ads_playlist[this.state.current_ad_index].video, { md5: true });
                    console.log(info)
                    // if (this.state.ads_playlist[this.state.current_ad_index].type == 1) {
                    this.setState({ ad_type: 1 }, async () => {
                        await this.player.unloadAsync();
                        await this.player.loadAsync({ uri: FileSystem.documentDirectory + "files/" + this.state.ads_playlist[this.state.current_ad_index].video }, { shouldPlay: true });
                    });
                    // } else {
                    // this.playImage();
                    // }
                }
            });
        } else {
            // do nothing for now
        }
    }

    didJustFinish(status) {
        if (status.didJustFinish) {
            let ad = this.state.ads_playlist[this.state.current_ad_index];
            let index = this.state.current_ad_index;
            this.updateAdStatus(ad);
            index += 1;
            if (this.state.ads_playlist.length - 1 < index) {
                index = 0;
                ad = this.state.ads_playlist[index];
            }
            this.setState({ current_ad_index: index, is_playing: false }, async () => {
                await this.play()
            });
        }
    }

    // async playImage() {
    //     let ad = this.state.ads_playlist[this.state.current_ad_index];
    //     const file = FileSystem.documentDirectory + "files/" + ad.file_name;
    //     this.setState({ current_ad_image: file, ad_type: 0 }, () => {
    //         setTimeout(() => {
    //             // this.state.ads_playlist.map(e => {
    //             //     if (e.md5_checksum == ad.md5_checksum) {

    //             //     }
    //             // })
    //             console.log("timeout playing next index in playlist");
    //             let index = this.state.current_ad_index;
    //             this.updateAdStatus(ad);
    //             index += 1;
    //             if (this.state.ads_playlist.length - 1 < index) {
    //                 index = 0;
    //                 ad = this.state.ads_playlist[index];
    //             }
    //             this.setState({ current_ad_index: index, is_playing: false }, async () => {
    //                 await this.play();
    //             });
    //         }, ad.duration);
    //     });
    // }

    async updateAdStatus(ad) {
        console.log('update')
        // let user_ads = this.state.user_ads;
        // if (user_ads.filter(e => e.ad_id == ad.id)[0] != undefined) {
        //     let user_ad = user_ads.filter(e => e.ad_id == ad.id)[0];
        //     if (user_ad.loops < ad.loops) {
        //         user_ad.loops = user_ad.loops + 1;
        //     }
        //     if (user_ad.loops == ad.loops) {
        //         user_ad.status = 1;
        //     } else {
        //         user_ad.status = 0;
        //     }
        //     user_ads = user_ads.map(e => {
        //         if (e.ad_id = ad.id) {
        //             return user_ad;
        //         }
        //     })
        //     await AsyncStorage.setItem("user_ads", JSON.stringify(user_ads))
        // } else {
        //     const user_ad = {
        //         ad_id: ad.id,
        //         loops: 1,
        //         status: 0,
        //     }
        //     user_ads.push(user_ad);
        //     await AsyncStorage.setItem("user_ads", JSON.stringify(user_ads))
        // }

        // const res = await fetch(this.backend_url + "/api/ads/update", {
        //     headers: {
        //         "Authorization": this.token,
        //         "Content-Type": "application/json"
        //     },
        //     method: "POST",
        //     body: JSON.stringify(ad)
        // }).then(res => res.json())
        // console.log('res', res);

    }

    constructor() {
        super();
        ScreenOrientation.allowAsync(ScreenOrientation.Orientation.LANDSCAPE);
    }

    componentDidMount() {
        this.load();
    }

    render() {
        const { width, height } = Dimensions.get('window');

        return (
            <TouchableWithoutFeedback style={styles.container}
                onPress={() => {
                    this.setState({ showMenu: true })
                }}>
                <View style={styles.container}>

                    <StatusBar hidden={true} />
                    {this.state.loading == true ?
                        <CustomProgressBar text={this.state.loading_text} />
                        :
                        this.state.ad_type == 1 ? <Video ref={(ref) => {
                            this.player = ref
                        }}
                            source={{ uri: this.state.src }}
                            shouldPlay={false}
                            resizeMode="contain"
                            onError={(error) => { console.log(error) }}
                            style={{ width, height }}
                            useNativeControls={false}
                            onPlaybackStatusUpdate={this.didJustFinish.bind(this)}
                        /> : <Image style={{ width, height }} resizeMode="contain" source={{ uri: this.state.current_ad_image }} />}
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const CustomProgressBar = ({ visible, text }) => (
    <Modal onRequestClose={() => null} visible={visible}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', width: Dimensions.get('window').width, height: Dimensions.get('window').height }}>
            <View style={{ borderRadius: 10, padding: 25, flexDirection: "row" }}>
                <ActivityIndicator size="large" />
                <Text style={{ marginLeft: 5, fontSize: 20, fontWeight: '200' }}>{text}</Text>
            </View>
        </View>
    </Modal>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    },
});