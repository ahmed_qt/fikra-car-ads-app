import React from 'react';
import { Modal, ActivityIndicator, StyleSheet, TouchableOpacity, Keyboard, TouchableWithoutFeedback, KeyboardAvoidingView, TextInput, Text, SafeAreaView, Image, View, Dimensions, StatusBar, NetInfo, AsyncStorage } from 'react-native';
import { Icon, Input } from 'native-base';
import { Video, FileSystem, ScreenOrientation } from 'expo';
import { showMessage, hideMessage } from "react-native-flash-message";
import FlashMessage from "react-native-flash-message";
import myconfig from '../config';

export default class LoginScreen extends React.Component {

    state = {
        loading: false,
        email: '',
        password: ''
    }
    constructor() {
        super();
        ScreenOrientation.allowAsync(ScreenOrientation.Orientation.PORTRAIT);
        this.load();
    }

    async load() {
        token = await AsyncStorage.getItem('token')
        if (token) {
            console.log(token);
            // need some validate works later
            this.props.navigation.navigate('Home')
        }
    }

    render() {
        const { width, height } = Dimensions.get('window');

        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content"></StatusBar>
                <KeyboardAvoidingView behavior="padding" style={styles.container}>
                    <TouchableWithoutFeedback style={styles.container}
                        onPress={Keyboard.dismiss}>
                        <View style={styles.container}>
                            <View style={styles.logoContainer}>
                                <Text style={styles.logo}>CarAds</Text>
                                <Text style={styles.subTitle}>Account Information</Text>
                            </View>
                            <View style={styles.inputContainer}>
                                <TextInput style={styles.input}
                                    placeholder="Enter your email"
                                    placeholderTextColor="rgba(255, 255, 255, 0.8)"
                                    keyboardType="email-address"
                                    returnKeyType="next"
                                    autoCorrect={false}
                                    value={this.state.email}
                                    onChangeText={(text) => { this.setState({ email: text }) }}
                                    editable={!this.state.loading}
                                    onSubmitEditing={() => this.refs.passwordInput.focus()} />
                                <TextInput style={styles.input}
                                    placeholder="Enter Password"
                                    placeholderTextColor="rgba(255, 255, 255, 0.8)"
                                    secureTextEntry
                                    returnKeyType="go"
                                    value={this.state.password}
                                    onChangeText={(text) => { this.setState({ password: text }) }}
                                    autoCorrect={false}
                                    editable={!this.state.loading}
                                    ref={"passwordInput"} />
                                <TouchableOpacity style={styles.buttonContainer}
                                    onPress={() => {
                                        if (this.state.loading) return;
                                        console.log('login clicked')
                                        this.setState({ loading: true })
                                        const data = { email: this.state.email, password: this.state.password }
                                        fetch(myconfig.backend_url + '/api/user/login',
                                            { method: 'post', body: JSON.stringify(data), headers: new Headers({ 'content-type': 'application/json' }) })
                                            .then((res) => {
                                                if (res.ok) {
                                                    return res.json();
                                                } else {
                                                    throw new Error('Something went wrong');
                                                }
                                            })
                                            .then(async res => {
                                                console.log('success!')
                                                showMessage({
                                                    message: "You have successfully loged in",
                                                    type: "success",
                                                })
                                                await AsyncStorage.setItem('token', res.token);
                                                this.setState({ loading: false })
                                            })
                                            .catch(err => {
                                                showMessage({
                                                    message: "No!! wrong Email or Password",
                                                    type: "danger",
                                                })
                                                console.log('noooo!')
                                                console.log(err)
                                                this.setState({ loading: false })
                                            })

                                    }}>
                                    {this.state.loading ? <ActivityIndicator size="small" color="#000" ></ActivityIndicator> : <Text style={styles.buttonText}>SIGN IN</Text>}
                                </TouchableOpacity>

                            </View>

                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
                <FlashMessage position="top" />
            </SafeAreaView>

        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgb(32, 53, 70)',
        flexDirection: 'column'
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    logoImage: {
    },
    logo: {
        color: '#ffff',
        fontSize: 30
    },
    subTitle: {
        color: '#71745E',
        fontSize: 18,
        marginTop: 5
    },
    inputContainer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        // backgroundColor: 'rgb(22, 43, 60)',
        height: 200,
        padding: 20,
        marginBottom: 20
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        color: '#fff',
        marginBottom: 20,
        paddingHorizontal: 10
    },
    buttonContainer: {
        backgroundColor: '#f7c744',
        paddingVertical: 15,
        height: 55,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        textAlign: 'center',
        color: 'rgb(32, 53, 70)',
        fontWeight: 'bold',
        fontSize: 18
    }
});